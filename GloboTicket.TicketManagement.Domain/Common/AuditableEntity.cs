﻿using System;

namespace GloboTicket.TicketManagement.Domain.Common
{
    /// <summary>
    /// This class include some common properties
    /// This class will be inherited by other entity classes
    /// No big logic here!!!
    /// </summary>
    public class AuditableEntity
    {
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
    }
}
