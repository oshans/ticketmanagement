﻿namespace GloboTicket.TicketManagement.Application.Models.Mail
{
    /// <summary>
    /// Email is not a core business entity
    /// Therefore, it can't go under Domain project's Entities
    /// we just need this to work Email service correctly
    /// </summary>
    public class Email
    {
        public string to { get; set; }
        public string subject { get; set; }
        public string body { get; set; }
    }
}