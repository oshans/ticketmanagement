﻿using MediatR;
using System;

namespace GloboTicket.TicketManagement.Application.Features.Events.Queries.GetEventDetail
{
    public class GetEventDetailQuery: IRequest<EventDetailVm>
    {
        /// <summary>
        /// This parameter specify which item need to be processed
        /// Event ID
        /// </summary>
        public Guid Id { get; set; }
    }
}
