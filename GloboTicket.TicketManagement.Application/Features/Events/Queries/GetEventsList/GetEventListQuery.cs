﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GloboTicket.TicketManagement.Application.Features.Events
{
    /// <summary>
    /// EventListVm --> just a class to return only required properties
    /// So we don't return too much data to client
    /// </summary>
    public class GetEventListQuery : IRequest<List<EventListVm>>
    {
    }
}
