﻿using GloboTicket.TicketManagement.Application.Responses;

namespace GloboTicket.TicketManagement.Application.Features.Categories.Commands.CreateCateogry
{
    /// <summary>
    /// This reponse class returns more data to client
    /// May include: Category info, Error list etc..
    /// BaseResponse: is a custom class
    /// </summary>
    public class CreateCategoryCommandResponse: BaseResponse
    {
        public CreateCategoryCommandResponse(): base()
        {

        }

        /// <summary>
        /// This is the returning type
        /// Newly created CreateCategoryDto
        /// </summary>
        public CreateCategoryDto Category { get; set; }
    }
}